
If you want to help the GNOME project, there are a number of things
you can do to help in the project: Everybody can help this project. 

We have divided the tasks in two parts: what programmers can do and
what non-programmers can do.

1. If you are a programmer
--------------------------

* Improve existing applications

Various of our applications can be improved to have more features,
better integration of the system and export their internal APIS trough
CORBA.

* New GNOME applications

To make your application a GNOME application, we request that you
release your code under a free license (GPL, MPL, NPL, LGPL, X11).  We
personally preffer GPL or LGPL, as it will allow us to reuse your code
and it will enable you to reuse parts of other GNOME code or code
licensed under the GPL.

We have a number of suggestions on how you can write your
application. Discussing new applications in gnome-devel-list@gnome.org
is usually a good idea, as you will be able to get input from other
developers and even get people interested in your project.

* Bug fixing existing applications

If you find a bug and you can fix it, we would welcome patches to the
existing code base.  Fixing bugs is a continuos process in any
software development project.  


2. If you are not a programmer
------------------------------

* Bug reporting

If you find a problem in a GNOME application, please try to provide a
good bug report: a way to reproduce the bug is a good starting point.
Provide details as your OS, the libraries and versions of the code you
are using.

The gnome-bug program provides a simple way of doing this.

If the program crashes, and if possible, provide a stack trace of the
program at the point where it crashes.  To do so, run your program
like this:

	gdb program core

Where core is the file generated from the crash, and run the "where"
command, provide this in your bug report.

* Documentation

Improving and updating the GNOME documentation is a continous effort.
If you find problems or incomplete documentation, we would gladly
accept any changes, improvements and new additions to our
documentation system.


* Translations

GNOME has support for working in a various languages.  The process of
adding more languages to GNOME and to improve the existing translation
and localizations is a continuos effort.

Contact your regional language team (2-letter iso code at li.org, like
this: es@li.org).  

If you are interested in this, read the documentation in Info format
for the gettext package (info:(gettext)).

3. If you are an artist
-----------------------

We could use both sounds and icons for providing various themes for
the desktop.  Contact the gnome-devel-list for more information on
what sort of thing you could provide.

Miguel